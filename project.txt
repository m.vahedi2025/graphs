import java.util.ArrayList;
import java.util.Scanner;
class Main
{

	int V;
	int x;
	ArrayList<ArrayList<Integer> > adjListArray;


	Main(int V)
	{
		this.V = V;
		
		adjListArray = new ArrayList<>();

		for (int i = 0; i < V; i++) {
			adjListArray.add(i, new ArrayList<>());
		}
	}

	void addEdge(int src, int dest)
	{
		adjListArray.get(src).add(dest);

		adjListArray.get(dest).add(src);
	}

	void DFSUtil(int v, boolean[] visited)
	{
		visited[v] = true;
		System.out.print(v + " ");
		for (int x : adjListArray.get(v)) {
			if (!visited[x])
				DFSUtil(x, visited);
		}
	}
	void connectedComponents()
	{
		boolean[] visited = new boolean[V];
		for (int v = 0; v < V; ++v) {
			if (!visited[v]) {
				DFSUtil(v, visited);
				System.out.println();
				x++;
			}
		}
		System.out.println("Number of connected components");
		System.out.println((x));
	}

	public static void main(String[] args)
	{
        Scanner reader=new Scanner(System.in);
		System.out.print("Enter Number of vertices");
		int V=reader.nextInt();
		Main g = new Main(V); 
		
		System.out.print("Enter Number of Edges");
		int E=reader.nextInt();
		for (int i=E ; i>0 ; i--){
		    
		    System.out.print("Enter vertices that are connected");
		    int n=reader.nextInt();
		    int m=reader.nextInt();
		    g.addEdge(n, m);
		    
		    
		}
        

		System.out.println("Following are connected components");
		
		g.connectedComponents();
	}
}
